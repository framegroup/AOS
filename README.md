### 介绍
PanGu Dev Framework是[普蓝开源](https://gitee.com/pulanos)生态下的一个简单、纯洁的技术开发框架。代号：盘古技术开发框架。

### :heartbeat: 彩蛋
##### 阿里开源技术交流微信群（ _微信扫码加入_ ）
![输入图片说明](https://images.gitee.com/uploads/images/2021/0619/020708_d3a7e67e_431745.jpeg "wechat2.jpeg")  
 :underage: 希望在这里和大家共建一个持续分享阿里相关开源技术和最佳参考实践的社群。「重点关注项目：Dubbo、Nacos、Seata、Sentinel」【分享经验-传递价值-成就你我】:100: 

